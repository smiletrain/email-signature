import '../App.css';
import Form from './Form.js'

function App() {
  return (
    <div className="App">
      <h1>Smile Train Signature Generator</h1>
      <Form />
    </div>
  );
}

export default App;
