import React, { Component, Fragment } from 'react'
import '../App.css'

export default class FormResult extends Component {
  handleClick = (ev) => {
    const copyBoxElement = this.copyBoxRef.current
    copyBoxElement.contentEditable = true
    copyBoxElement.focus()
    document.execCommand('selectAll')
    document.execCommand('copy')
    copyBoxElement.contentEditable = false
    getSelection().empty()
    this.changeText('Copied')
    setTimeout(() => {
      this.changeText('Copy Signature')
    }, 1000)
  }

  copyBoxRef = React.createRef()

  state = {
    text: 'Copy Signature',
  }

  changeText = (text) => {
    this.setState({ text })
  }

  render() {
    const { text } = this.state //destucture state

    return (
      <div className='form-result'>
        <h2>Signature Result</h2>
        <div
          style={{
            border: '1px solid #cfcfcf',
            padding: '20px 34px',
            borderRadius: 4,
            background: 'none',
          }}
          className='copybox'
          ref={this.copyBoxRef}
        >
          <div>
            <table
              width='auto'
              cellSpacing='0'
              cellPadding='0'
              bgcolor='none'
              style={{
                fontFamily: 'arial, sans-serif',
                fontSize: 12,
                color: '#626165',
                background: 'none',
              }}
            >
              <tbody>
                {/* NAME */}

                {this.props.name && this.props.pronouns ? (
                  <tr>
                    <td style={{ fontWeight: 900, fontSize: 14, padding: 0 }}>
                      {this.props.name} <span style={{ fontWeight: 500 }}>({this.props.pronouns})</span>
                    </td>
                  </tr>
                ) : (
                  <Fragment></Fragment>
                )}

                {this.props.name && !this.props.pronouns ? (
                  <tr>
                    <td style={{ fontWeight: 900, fontSize: 14, padding: 0 }}>
                      {this.props.name}
                    </td>

                  </tr>
                ) : (
                  <Fragment></Fragment>
                )}


                {/* TITLE */}
                {this.props.title ? (
                  <tr>
                    <td
                      style={{
                        fontSize: 14,
                        borderBottom: '2px solid #8d8c8f',
                        padding: '0 0 6px 0',
                      }}
                    >
                      {this.props.title}
                    </td>
                  </tr>
                ) : (
                  <Fragment></Fragment>
                )}

                {/* PHONE */}
                {this.props.phone ? (
                  <tr>
                    <td style={{ padding: '8px 0 0 0' }}>{this.props.phone}</td>
                  </tr>
                ) : (
                  <Fragment></Fragment>
                )}

                {/* ADDRESS */}
                {this.props.region ? (
                  <Fragment>
                    {regions[this.props.region].address.map((object, i) => {
                      return !this.props.phone && i === 0 ? (
                        <tr>
                          <td style={{ padding: '8px 0 0 0' }}>{object}</td>
                        </tr>
                      ) : (
                        <tr>
                          <td style={{ padding: 0 }}>{object}</td>
                        </tr>
                      )
                    })}
                  </Fragment>
                ) : (
                  <Fragment></Fragment>
                )}

                {/* WEBSITE */}
                {this.props.region ? (
                  <tr>
                    <td style={{ padding: 0 }}>
                      <a
                        style={{ textDecoration: 'none', color: '#626165' }}
                        href={regions[this.props.region].websiteURL}
                      >
                        {regions[this.props.region].websiteDisplayURL}
                      </a>
                    </td>
                  </tr>
                ) : (
                  <Fragment></Fragment>
                )}

                {/* LOGO */}
                <tr>
                  <td>
                    {this.props.region === 'Simulare' ? (
                      <img width='100' src='/assets/simulare-logo.png' alt='' />
                    ) : (
                      <img width='100' src='/assets/logo.png' alt='' />
                    )}
                  </td>
                </tr>

                {/* QUOTE */}
                {this.props.showQuote === 'yes' &&
                this.props.region === 'USA' ? (
                  <Fragment>
                    <tr>
                      <td>
                        <a
                          style={{ textDecoration: 'none', color: '#626165' }}
                          href='https://www.smiletrain.org/awards-ratings?utm_source=email-signature&utm_medium=email&utm_campaign=signature2021&utm_content=awards-page-link'
                        >
                          <i>Give with confidence.</i>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <a
                          style={{ textDecoration: 'none', color: '#626165' }}
                          href='https://www.smiletrain.org/awards-ratings?utm_source=email-signature&utm_medium=email&utm_campaign=signature2021&utm_content=awards-page-link'
                        >
                          Learn how Smile Train sets the standard of excellence.
                        </a>
                      </td>
                    </tr>
                  </Fragment>
                ) : (
                  <Fragment></Fragment>
                )}

                {/* SOCIAL MEDIA ICONS */}
                {this.props.showIcons === 'yes' && this.props.region ? (
                  <Fragment>
                    <tr height='6'></tr>
                    <tr>
                      <td>
                        <table cellPadding='0' cellSpacing='0'>
                          <tbody>
                            <tr>
                              {/* Facebook */}

                              {regions[this.props.region].socialMedia
                                .facebook ? (
                                <td>
                                  <a
                                    href={
                                      regions[this.props.region].socialMedia
                                        .facebook
                                    }
                                  >
                                    <img
                                      height='14'
                                      src='/assets/facebook.png'
                                      alt=''
                                    />
                                  </a>
                                  &nbsp;&nbsp;&nbsp;
                                </td>
                              ) : (
                                <Fragment></Fragment>
                              )}

                              {/* Twitter */}
                              {regions[this.props.region].socialMedia
                                .twitter ? (
                                <td>
                                  <a
                                    href={
                                      regions[this.props.region].socialMedia
                                        .twitter
                                    }
                                  >
                                    <img
                                      height='11'
                                      src='/assets/twitter.png'
                                      alt=''
                                    />
                                  </a>
                                  &nbsp;&nbsp;&nbsp;
                                </td>
                              ) : (
                                <Fragment></Fragment>
                              )}

                              {/* Youtube */}
                              {regions[this.props.region].socialMedia
                                .youtube ? (
                                <td>
                                  <a
                                    href={
                                      regions[this.props.region].socialMedia
                                        .youtube
                                    }
                                  >
                                    <img
                                      height='11'
                                      src='/assets/youtube.png'
                                      alt=''
                                    />
                                  </a>
                                  &nbsp;&nbsp;&nbsp;
                                </td>
                              ) : (
                                <Fragment></Fragment>
                              )}

                              {/* Instagram */}
                              {regions[this.props.region].socialMedia
                                .instagram ? (
                                <td>
                                  <a
                                    href={
                                      regions[this.props.region].socialMedia
                                        .instagram
                                    }
                                  >
                                    <img
                                      height='14'
                                      src='/assets/instagram.png'
                                      alt=''
                                    />
                                  </a>
                                  &nbsp;&nbsp;&nbsp;
                                </td>
                              ) : (
                                <Fragment></Fragment>
                              )}

                              {/* Linkedin */}
                              {regions[this.props.region].socialMedia
                                .linkedin ? (
                                <td>
                                  <a
                                    href={
                                      regions[this.props.region].socialMedia
                                        .linkedin
                                    }
                                  >
                                    <img
                                      height='12'
                                      src='/assets/linkedin.png'
                                      alt=''
                                    />
                                  </a>
                                </td>
                              ) : (
                                <Fragment></Fragment>
                              )}
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </Fragment>
                ) : (
                  <Fragment></Fragment>
                )}
              </tbody>
            </table>
          </div>
        </div>

        <button type='submit' onClick={this.handleClick}>
          {text}
        </button>

        <p style={{ textAlign: 'center', color: '#333' }}>
          <a
            style={{ color: '#333' }}
            rel='noopener noreferrer'
            target='_blank'
            href='https://smiletrain.notion.site/How-To-Email-Signature-47ca3c470a414248bdf41348b6421d73'
          >
            How to add email signature to Outlook
          </a>
        </p>
      </div>
    )
  }
}

const regions = {
  USA: {
    address: ['Smile Train', '633 Third Ave, 9th Floor', 'New York, NY 10017'],
    websiteURL:
      'https://www.smiletrain.org/?utm_source=email-signature&utm_medium=email&utm_campaign=signature',
    websiteDisplayURL: 'smiletrain.org',
    socialMedia: {
      facebook: 'https://smiletra.in/stfb',
      twitter: 'https://smiletra.in/sttwitter',
      youtube: 'https://smiletra.in/styoutube',
      instagram: 'https://smiletra.in/stinstagram',
      linkedin: 'https://smiletra.in/stlinkedin',
    },
  },
  UK: {
    address: ['Smile Train UK', 'York House', 'Wetherby Road York YO26 7NH UK'],
    websiteURL:
      'https://smiletrain.org.uk/?utm_source=email-signature&utm_medium=email&utm_campaign=signature',
    websiteDisplayURL: 'smiletrain.org.uk',
    socialMedia: {
      facebook: 'https://smiletra.in/stukfb',
      twitter: 'https://smiletra.in/stuktwitter',
      youtube: 'https://smiletra.in/stukyoutube',
      instagram: 'https://smiletra.in/stukinstagram',
    },
  },
  Africa: {
    address: [
      'Smile Train Africa',
      'Ngong Hills Business Center, 4th Floor',
      'Ngong Road',
      'PO Box 2512-00100',
      'Nairobi, Kenya',
    ],
    websiteURL:
      'https://www.smiletrainafrica.org/?utm_source=email-signature&utm_medium=email&utm_campaign=signature',
    websiteDisplayURL: 'smiletrainafrica.org',
    socialMedia: {
      facebook: 'https://smiletra.in/stafricafacebook',
      twitter: 'https://smiletra.in/stafricatwitter',
    },
  },
  India: {
    address: [
      'Smile Train India',
      'Plot No 3, LSC, Sector C​',
      'Pocket 6/7',
      'Vasant Kunj',
      'New Delhi 110070',
    ],
    websiteURL:
      'https://www.smiletrainindia.org/?utm_source=email-signature&utm_medium=email&utm_campaign=signature',
    websiteDisplayURL: 'smiletrainindia.org',
    socialMedia: {
      facebook: 'https://smiletra.in/stindiafb',
      twitter: 'https://smiletra.in/stindiatwitter',
      youtube: 'https://smiletra.in/stindiayoutube',
    },
  },
  Germany: {
    address: ['Smile Train Germany', 'Terminalstraße Mitte 1885356 München'],
    websiteURL:
      'https://smiletrain.de/?utm_source=email-signature&utm_medium=email&utm_campaign=signature',
    websiteDisplayURL: 'smiletrain.de',
    socialMedia: {
      facebook: 'https://smiletra.in/stdefb',
    },
  },
  Brasil: {
    address: [
      'Smile Train Brasil',
      'Av. Paulista, n.º 2.300, Ed. São Luis Gonzaga, andar Pilotis',
      'Bela Vista, São Paulo - SP',
      'CEP 01310-300',
    ],
    websiteURL:
      'https://www.smiletrainbrasil.com/?utm_source=email-signature&utm_medium=email&utm_campaign=signature',
    websiteDisplayURL: 'smiletrainbrasil.com',
    socialMedia: {
      facebook: 'https://smiletra.in/stbrfb',
      youtube: 'https://smiletra.in/stbryoutube',
      instagram: 'https://smiletra.in/stbrinstagram',
    },
  },
  LATAM: {
    address: [
      'Smile Train México, Central America & Caribbean',
      'Viena 161, Colonia Del Carmen',
      'Alcaldía Coyoacán, Ciudad de México, CDMX',
      'C.P. 04100',
    ],
    websiteURL:
      'https://www.smiletrainla.org/?utm_source=email-signature&utm_medium=email&utm_campaign=signature',
    websiteDisplayURL: 'smiletrainla.org',
    socialMedia: {
      facebook: 'https://smiletra.in/stlafb',
      twitter: 'https://smiletra.in/stlatwitter',
      instagram: 'https://smiletra.in/stlainstagram',
    },
  },
  MENA: {
    address: ['Smile Train Middle East & North Africa'],
    websiteURL:
      'https://www.smiletrainmena.org/?utm_source=email-signature&utm_medium=email&utm_campaign=signature',
    websiteDisplayURL: 'smiletrainmena.org',
    socialMedia: {
      facebook: 'https://smiletra.in/stdubaifb',
      twitter: 'https://smiletra.in/stdubaitwitter',
      instagram: 'https://smiletra.in/stdubaiinstagram',
    },
  },
  Indonesia: {
    address: [
      'Smile Train Indonesia',
      'Talavera Office Park, 28th Floor - Suite B 17',
      'Jl. T.B.Simatupang, Kav 22-26',
      'Jakarta 12430, Indonesia',
    ],
    websiteURL:
      'https://www.smiletrainindonesia.org/?utm_source=email-signature&utm_medium=email&utm_campaign=signature',
    websiteDisplayURL: 'smiletrainindonesia.org',
    socialMedia: {
      facebook: 'https://smiletra.in/stinfacebook',
      twitter: 'https://smiletra.in/stintwitter',
      instagram: 'https://smiletra.in/stininstagram',
    },
  },
  Philippines: {
    address: [
      'Smile Train Philippines',
      '3/F Annex Bldg., 22 East Ave.',
      'Diliman, Quezon City, Metro Manila',
      'Philippines',
    ],
    websiteURL:
      'https://www.smiletrain.ph/?utm_source=email-signature&utm_medium=email&utm_campaign=signature',
    websiteDisplayURL: 'smiletrain.ph',
    socialMedia: {
      facebook: 'https://smiletra.in/stphfb',
      twitter: 'https://smiletra.in/stphtwitter',
      instagram: 'https://smiletra.in/stphinstagram',
    },
  },
  Simulare: {
    address: [
      'Simulare Medical, a Division of Smile Train',
      '174 Spadina Ave, Suite 404',
      'Toronto, ON, M5T 2C2',
      'Canada',
    ],
    websiteURL:
      'https://www.smiletrain.org/simulare/?utm_source=email-signature&utm_medium=email&utm_campaign=signature',
    websiteDisplayURL: 'smiletrain.org/simulare',
    socialMedia: {
      twitter: 'https://twitter.com/STSimulare',
    },
  },
}
