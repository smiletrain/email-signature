import React, { Component, Fragment } from 'react'
import FormResult from './FormResult'
import '../App.css'

export default class Form extends Component {
  constructor() {
    super()
    this.state = {
      submitted: false,
      name: '',
      pronouns: '',
      title: '',
      phone: '',
      mobile: '',
      fax: '',
      region: '',
      showQuote: '',
      showIcons: '',
    }

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.setState((state) => ({
      submitted: false,
    }))
    this.setState({ [event.target.name]: event.target.value })
  }

  render() {
    return (
      <div className='main'>
        <div className='form'>
          <form>
            <h2>Enter Your Information</h2>
            <fieldset>
              {/* NAME */}
              <div>
                <label>Name</label>
                <input
                  name='name'
                  type='text'
                  id='name'
                  value={this.state.value}
                  onChange={this.handleChange}
                />
              </div>

               {/* PRONOUNS */}
               <div>
                <label>Pronouns (e.g. she/her/hers)</label>
                <input
                  name='pronouns'
                  type='text'
                  id='pronouns'
                  value={this.state.value}
                  onChange={this.handleChange}
                />
              </div>

              {/* TITLE */}
              <div>
                <label>Title</label>
                <input
                  name='title'
                  type='text'
                  id='title'
                  value={this.state.value}
                  onChange={this.handleChange}
                />
              </div>

              {/* PHONE NUMBER */}
              <div>
                <label>Phone</label>
                <input
                  name='phone'
                  type='text'
                  id='phone'
                  value={this.state.value}
                  onChange={this.handleChange}
                />
              </div>


              {/* REGION */}
              <div>
                <label>Region</label>
                <select
                  id='region'
                  name='region'
                  value={this.state.value}
                  onChange={this.handleChange}
                >
                  <option value='none' selected disabled hidden>
                    Select Region
                  </option>
                  <option value='USA'>USA</option>
                  <option value='UK'>UK</option>
                  <option value='Africa'>Africa</option>
                  <option value='India'>India</option>
                  <option value='Germany'>Germany</option>
                  <option value='Brasil'>Brasil</option>
                  <option value='LATAM'>LATAM</option>
                  <option value='MENA'>MENA</option>
                  <option value='Indonesia'>Indonesia</option>
                  <option value='Philippines'>Philippines</option>
                  <option value='Simulare'>Simulare</option>
                </select>
              </div>

              {/* SHOW QUOTE? */}

              {this.state.region === 'USA' ? (
                <div>
                  <label>Show quote?</label>

                  <div className='radio-container'>
                    <label>
                      <input
                        type='radio'
                        name='showQuote'
                        value='yes'
                        checked={this.state.showQuote === 'yes'}
                        onChange={this.handleChange}
                      />
                      Yes
                    </label>

                    <label>
                      <input
                        type='radio'
                        name='showQuote'
                        value='no'
                        checked={this.state.showQuote === 'no'}
                        onChange={this.handleChange}
                      />
                      No
                    </label>
                  </div>
                </div>
              ) : (
                <Fragment></Fragment>
              )}

              {/* SHOW SOCIAL MEDIA */}
              {this.state.region ? (
              <div>
                <label>Show social media icons?</label>

                <div className='radio-container'>
                  <label>
                    <input
                      type='radio'
                      name='showIcons'
                      value='yes'
                      checked={this.state.showIcons === 'yes'}
                      onChange={this.handleChange}
                    />
                    Yes
                  </label>

                  <label>
                    <input
                      type='radio'
                      name='showIcons'
                      value='no'
                      checked={this.state.showIcons === 'no'}
                      onChange={this.handleChange}
                    />
                    No
                  </label>
                </div>
              </div>
              ) : (
                <Fragment></Fragment>
              )}

            </fieldset>
            <br />
          </form>
        </div>
        {/* if submitted is false, don't show FormResult, else do not show it */}
        <FormResult
          submitted={this.state.submitted}
          name={this.state.name}
          pronouns={this.state.pronouns}
          title={this.state.title}
          phone={this.state.phone}
          region={this.state.region}
          showQuote={this.state.showQuote}
          showIcons={this.state.showIcons}
        />
      </div>
    )
  }
}
